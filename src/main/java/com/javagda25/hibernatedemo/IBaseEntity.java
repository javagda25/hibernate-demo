package com.javagda25.hibernatedemo;

public interface IBaseEntity {
    Long getId();
}
